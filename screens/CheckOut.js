import React, { Component } from 'react';
import { StyleSheet, Text,  View, TouchableOpacity, Image, SafeAreaView, Alert, ScrollView} from 'react-native';
import { Card } from 'react-native-paper';

export default class CheckOut extends Component{
    constructor(props) {
        super(props);
        this.state = {
          time: new Date().toLocaleString(),
          item:''
        };
      }

      componentDidMount(props) {
        console.log(this.props.route.params.item);
      }
      
      componentWillUnmount() {
        clearInterval(this.intervalID);
      }
      tick() {
        this.setState({
          time: new Date().toLocaleString()
        });
      }

    createAlert = () =>
    Alert.alert(
      "six30.frosec.com says",
      "Are you sure?",
      [
        {
          text: "Cancel",
          onPress: () => this.props.navigation.navigate("Welcome"),
          style: "cancel"
        },
        { text: "Ok", onPress: () => this.props.navigation.navigate("ThankYou") }
      ],
      { cancelable: true }
    );

    render(props){
        return(
            <ScrollView>
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    <Card style={styles.cardView}>
                        <View style={{ fontWeight: 'bold', fontSize: 15, paddingBottom: 10, flexDirection: 'row', marginHorizontal: 1, justifyContent: 'space-between'}}>
                            <Text style={{ fontWeight: 'bold', fontSize: 18}}> George Court</Text>
                                <Image
                                    style={{
                                    height: 50,
                                    width: 55
                                    }}
                                    size={100}
                                    source={require("../Images/logo.png")}
                                /> 
           
                        </View>
                        {/* <View style= {{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Image 
                                style={{
                                    height:80,
                                    width:80,
                                }}
                                source={require("../Images/user.png")}
                                source={{uri: this.props.route.params.uri}}
                            />
                        </View> */}
                        <View>
                            <Text style={{ fontWeight: 'bold', fontSize: 14}}> Name: {this.props.route.params.item.emp_name}</Text>
                        </View>
                        <View>
                            <Text style={{ fontWeight: 'bold', fontSize: 14, paddingBottom: 20, paddingTop: 20}}> Contact No(Mobile): {this.props.route.params.item.emp_contact} </Text>
                        </View>
                        <View>
                          <Text style={{ fontWeight: 'bold', fontSize: 14}}> Date and Time: {this.state.time} </Text>
                        </View>
                    </Card> 
                    <View style={{ paddingBottom:50}}></View> 
                <TouchableOpacity
                    style = {styles.submitButton} onPress={this.createAlert}>
                    <Text style = {styles.submitButtonText}> CheckOut </Text>
                </TouchableOpacity>
            </View>
            </SafeAreaView>
            </ScrollView>
        );
    }
}


const styles = StyleSheet.create({
    container:{
        flex:1,
        paddingTop: 50
    },
    cardView:{
        margin: 8,
        padding: 30,
        paddingTop: 20, 
        backgroundColor:"white",
        borderRadius:15,
        elevation:10,
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 10,
        shadowRadius: 5, 
    },
    submitButton: {
        backgroundColor: '#40e0d0',
        borderRadius: 10,
        padding: 10,
        margin: 10,
        marginLeft: 10,
        height: 40
    },
    submitButtonText:{
        color: '#000000',
        textAlign: 'center',
    }
}
);