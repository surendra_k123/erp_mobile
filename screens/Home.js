import React, { Component, Fragment } from 'react';
import { TouchableOpacity, View, Text, StyleSheet } from 'react-native';
import SearchableDropdown from 'react-native-searchable-dropdown';

const items = [
  { name: '9645991037', emp_name: 'Vishnu Prabhalan', emp_contact: 9645991037},
  { name: '8553204380', emp_name: 'Vinodh K', emp_contact: 8553204380 },
  // { name: '980961XXXX', emp_name: 'Sreejith S', emp_contact: 8553204380 },
  { name: '9556233255', emp_name: 'Sarthak Pujari', emp_contact: 9556233255 },
  // { name: '811303XXXX', emp_name: 'Rohit', emp_contact: },
  { name: '9844296885', emp_name: 'Raghavendra Nairy', emp_contact: 9844296885},
  // { name: '741107XXXX', emp_name: 'Prasanna Acharaya'},
  { name: '9844921616', emp_name: 'Girish K V', emp_contact: 9844921616 },
  // { name: '973116XXXX', emp_name: 'Dinesh Naik', emp_contact: 8553204380 },
  { name: '8301012034', emp_name: 'Arya Regunath', emp_contact: 8301012034},
  // { name: '953522XXXX', emp_name: 'Arogya Justin'},
  { name: '9447719366', emp_name: 'Abraham George', emp_contact: 9447719366},
  { name: '9074277033', emp_name: 'Sanju Sunny', emp_contact: 9074277033},
  { name: '9986021790', emp_name: 'Sreenath Sreedharan', emp_contact: 9986021790},
  { name: '6282952623', emp_name: 'Abishek P S', emp_contact: 6282952623},
  { name: '9620360635', emp_name: 'Surendra', emp_contact: 9620360635},
  { name: '8962406812', emp_name: 'Prashant Kumar', emp_contact: 8962406812}, 
];

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedItems: []
    }
  }

  render(props) {
    return (
      <Fragment>
        <View style={{ flex: 1 }}>
          <Text style={{
            paddingHorizontal: 30, textAlign: 'right', marginTop: 10,
            fontWeight: 'bold', fontSize: 16, paddingBottom: 150
          }}> STEP  1/6 </Text>
          <Text style={styles.titleText}>
            Employee Contact Number*
            </Text>
          <SearchableDropdown
            multi={true}
            selectedItems={this.state.selectedItems}
            onItemSelect={(item) => {
              const items = this.state.selectedItems;
              items.push(item)
              this.setState({ selectedItems: items });
              console.log(item);
                this.props.navigation.navigate('CheckOut', { item: item });
                this.props.navigation.navigate('ProfileForms2', { item: item });
                this.props.navigation.navigate('ProfileDetails', { item: item });
                this.props.navigation.navigate('Camera', { item: item });
              }}
            containerStyle={{ padding: 5 }}
            onRemoveItem={(item, index) => {
              const items = this.state.selectedItems.filter((sitem) => sitem.id !== item.id);
              this.setState({ selectedItems: items });
            }}
            itemStyle={{
              padding: 10,
              marginTop: 2,
              backgroundColor: '#FAF9F8',
              borderColor: '#bbb',
              borderWidth: 1,
              borderRadius: 5,
            }}
            itemTextStyle={{ color: '#222' }}
            itemsContainerStyle={{ maxHeight: 180 }}
            items={items}
            defaultIndex={12}
            chip={true}
            resetValue={false}
            textInputProps={
              {
                placeholder: "Select Employee Name and PhoneNumber",
                underlineColorAndroid: "transparent",
                style: {
                  padding: 12,
                  borderWidth: 1,
                  borderColor: '#ccc',
                  borderRadius: 5,
                },
                // onTextChange: text => alert(text)
              }
            }
            listProps={
              {
                nestedScrollEnabled: true,
              }
            }
          />
          <View style={{ flex: 1 }}>
            <TouchableOpacity
              style={styles.submitButton}
              onPress={() => this.props.navigation.navigate('Camera')}>
              <Text style={styles.submitButtonText}> Next </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10,
  },
  titleText: {
    padding: 10,
    fontSize: 18,
    textAlign: 'left',
    fontWeight: 'bold',
  },
  submitButton: {
    backgroundColor: '#40e0d0',
    borderRadius: 10,
    padding: 10,
    margin: 5,
    height: 40,
  },
  submitButtonText: {
    color: '#000000',
    textAlign: 'center',
  }
});